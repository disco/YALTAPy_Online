# YALTAPy_Online

Catherine Bonnet, Guilherme Mazanti, Jayvir Raj

Version 1.0.0 &mdash; August 2021

## Running YALTAPy_Online

YALTAPy_Online is a Jupyter Notebook that can be easily run through [binder](https://mybinder.org/) by clicking on the button below:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fdisco%2FYALTAPy_Online/HEAD?urlpath=tree%2FYALTAPy_Online.ipynb)

The page may take a few minutes to load. Once YALTAPy_Online is loaded, click on the "Kernel" menu, choose the option "Restart & Run All", and confirm by clicking on the red button "Restart and Run All Cells". You will then see the main screen of YALTAPy_Online.

## About YALTAPy_Online

YALTAPy_Online is based on [YALTAPy](https://project.inria.fr/yalta/), a Python toolbox dedicated to the stability analysis of (possibly fractional) delay systems with commensurate delays given by their transfer function. More details on YALTAPy can be found on its [website](https://project.inria.fr/yalta/), its [Gitlab repository](https://gitlab.inria.fr/disco/yaltapy-public), and its [PyPI page](https://pypi.org/project/yaltapy/).

YALTAPy_Online is structured as a Jupyter Notebook and allows one to easily use all of YALTAPy functionalities thanks to its friendly user interface built using interactive widgets from Python's ipywidgets and Qgrid module package. More information on YALTAPy_Online can be found in the following publication:

- H. Cavalera, J. Raj, G. Mazanti, C. Bonnet. [YALTAPy and YALTAPy_Online: Python toolboxes for the H<sub>∞</sub>-stability analysis of classical and fractional systems with commensurate delays.](https://doi.org/10.1016/j.ifacol.2022.11.356) 17th IFAC Workshop on Time Delay Systems (TDS 2022). *IFAC-PapersOnLine*, 55(36):192&ndash;197, 2022.